package koshchiy.tankgame.server;

import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.handler.codec.http.*;
import io.netty.util.CharsetUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Map;

import static io.netty.handler.codec.http.HttpHeaders.Names.CONTENT_TYPE;
import static io.netty.handler.codec.http.HttpMethod.GET;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.HTTP_1_1;
import static io.netty.handler.codec.http.LastHttpContent.EMPTY_LAST_CONTENT;


class HttpStaticFileHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private final String resourcesPath;

    private static Map<String, String> mimeTypes;
    static {
        mimeTypes = new HashMap<>();
        mimeTypes.put("js", "application/javascript");
        mimeTypes.put("html", "text/html");
        mimeTypes.put("css", "text/css");
        mimeTypes.put("svg", "image/svg+xml");
        mimeTypes.put("ttf", "application/x-font-ttf");
        mimeTypes.put("otf", "application/x-font-opentype");
        mimeTypes.put("woff", "application/font-woff");
        mimeTypes.put("eot", "application/vnd.ms-fontobject");
    }

    public HttpStaticFileHandler(String resourcesPath) {
        this.resourcesPath = resourcesPath;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest msg) throws Exception {
        if (msg.getMethod() != GET) {
            sendError(ctx, FORBIDDEN);
            return;
        }
        String uri = msg.getUri();
        if (uri.equals("/")) {
            sendFile(ctx, msg, getPath("/index.html"));
        } else if (uri.startsWith("/websocket")) {
            ctx.fireChannelRead(msg);
        } else {
            sendFile(ctx, msg, getPath(uri));
        }
    }

    private void sendFile(ChannelHandlerContext ctx, FullHttpRequest request, String path) throws Exception {
        File file = new File(path);
        if (!file.exists()) {
            sendError(ctx, NOT_FOUND);
            return;
        }

        RandomAccessFile raf = new RandomAccessFile(file, "r");
        long fileLength = raf.length();
        HttpResponse response = new DefaultHttpResponse(HTTP_1_1, OK);
        HttpHeaders.setContentLength(response, fileLength);
        setContentType(response, file.getName());

        ctx.write(response);
        ctx.write(ctx.write(new DefaultFileRegion(raf.getChannel(), 0, fileLength)));

        ChannelFuture lastContentFuture = ctx.writeAndFlush(EMPTY_LAST_CONTENT);
        lastContentFuture.addListener(ChannelFutureListener.CLOSE);
    }

    private void sendError(ChannelHandlerContext ctx, HttpResponseStatus status) {
        FullHttpResponse response = new DefaultFullHttpResponse(
            HTTP_1_1,
            status,
            Unpooled.copiedBuffer("Failure: " + status.toString() + "\r\n",CharsetUtil.UTF_8)
        );

        response.headers().set(CONTENT_TYPE, "text/plain; charset=UTF-8");
        ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }

    private String getPath(String uri) {
        //TODO fix path security hole
        return this.resourcesPath + uri;
    }

    private void setContentType(HttpResponse response, String name) {
        String ext = name.substring(name.lastIndexOf('.') + 1);
        String mime = mimeTypes.getOrDefault(ext, "application/octet-stream");
        response.headers().set(CONTENT_TYPE, mime);
    }
}