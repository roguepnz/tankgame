package koshchiy.tankgame.server;

import koshchiy.tankgame.game.PlayerUpdate;

/**
 *  A player events listener.
 */
public interface ServerListener {

    /**
     * Called when the player {@link koshchiy.tankgame.game.PlayerUpdate update} is received.
     *
     * @param update received update
     */
    void playerUpdated(PlayerUpdate update);

    /**
     * Called when the player connected to the server.
     *
     * @param name name of the player
     */
    void playerConnected(String name);

    /**
     * Called when the player disconnected from the server.
     *
     * @param name name of the player
     */
    void playerDisconnected(String name);
}
