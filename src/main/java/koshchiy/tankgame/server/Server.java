package koshchiy.tankgame.server;

import koshchiy.tankgame.game.BattlefieldSnapshot;

/**
 * A game server interface.
 *
 * @see NettyServer
 */
public interface Server {

    /**
     * Starts a listening loop.
     */
    void run();

    /**
     * Sends {@link koshchiy.tankgame.game.BattlefieldSnapshot snapshot} to the player.
     *
     * @param name name of the player
     * @param snapshot snapshot to send
     */
    void sendSnapshot(String name, BattlefieldSnapshot snapshot);

    /**
     * Adds {@link ServerListener listener} to the server.
     *
     * @param listener listener to add
     */
    void addListener(ServerListener listener);

    /**
     * Removes {@link ServerListener listener} from the server.
     *
     * @param listener listener to remove
     */
    void removeListener(ServerListener listener);
}
