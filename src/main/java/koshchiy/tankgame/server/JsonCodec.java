package koshchiy.tankgame.server;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.json.JSONObject;

import java.util.List;

/**
 * Converts {@link io.netty.handler.codec.http.websocketx.TextWebSocketFrame text}
 * to {@link org.json.JSONObject jsonOjbect} and vice versa.
 */
class JsonCodec extends MessageToMessageCodec<TextWebSocketFrame, JSONObject> {

    @Override
    protected void encode(ChannelHandlerContext ctx, JSONObject msg, List<Object> out) throws Exception {
        out.add(new TextWebSocketFrame(msg.toString()));
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, TextWebSocketFrame msg, List<Object> out) throws Exception {
        out.add(new JSONObject(msg.text()));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        super.exceptionCaught(ctx, cause);
    }
}
