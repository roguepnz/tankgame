package koshchiy.tankgame.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import koshchiy.tankgame.game.BattlefieldSnapshot;
import koshchiy.tankgame.game.PlayerUpdate;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * {@link Server Server} implementation based on Netty Framework. Uses Websockets as transport layer.
 * On the same port serves websocket game requests and http requests for client hosting.
 *
 */
public class NettyServer implements Server {
    private final int port;
    private final String resourcesPath;

    private final Map<String, Channel> channels;
    private final Set<ServerListener> listeners;

    private final static Logger logger = LoggerFactory.getLogger(NettyServer.class);

    /**
     * Creates a new instance of the server.
     *
     * @param port port
     * @param resourcesPath absolute or relative path to the client files.
     */
    public NettyServer(int port, String resourcesPath) {
        this.resourcesPath = resourcesPath;
        this.port = port;

        this.listeners = new HashSet<>();
        this.channels = new HashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        NioEventLoopGroup group = new NioEventLoopGroup(1);
        try {
            ServerBootstrap bootstrap = new ServerBootstrap()
                .group(group)
                .channel(NioServerSocketChannel.class)
                .childHandler(new Initializer());

            bootstrap.bind(port).sync();
        } catch (Exception e) {
            group.shutdownGracefully();
            throw new RuntimeException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendSnapshot(String name, BattlefieldSnapshot snapshot) {
        channels.get(name).writeAndFlush(
            snapshot.put("type", MessageType.UPDATE.code())
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addListener(ServerListener listener) {
        this.listeners.add(listener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeListener(ServerListener listener) {
        this.listeners.remove(listener);
    }

    private void firePlayerUpdated(PlayerUpdate update) {
        this.listeners.forEach(l -> l.playerUpdated(update));
    }

    private void firePlayerDisconnected(String name) {
        this.listeners.forEach(l -> l.playerDisconnected(name));
    }

    private void firePlayerConnected(String name) {
        this.listeners.forEach(l -> l.playerConnected(name));
    }

    private class Initializer extends ChannelInitializer<SocketChannel> {

        @Override
        protected void initChannel(SocketChannel ch) throws Exception {
            ch.pipeline().addLast(
                new HttpRequestDecoder(),
                new HttpObjectAggregator(65536),
                new HttpResponseEncoder(),
                new HttpStaticFileHandler(resourcesPath),
                new WebSocketServerProtocolHandler("/websocket"),
                new JsonCodec(),
                new ClientHandler());
        }
    }

    /**
     * Serves client request messages. Performs authorization.
     */
    private class ClientHandler extends SimpleChannelInboundHandler<JSONObject> {
        private boolean isAuthorized;
        private String name;

        @Override
        protected void channelRead0(ChannelHandlerContext ctx, JSONObject msg) throws Exception {
            if (!isAuthorized) {
                authorize(ctx, msg);
            } else {
                handleRequest(ctx, msg);
            }
        }

        @Override
        public void channelInactive(ChannelHandlerContext ctx) throws Exception {
            if (isAuthorized) {
                channels.remove(name);
                firePlayerDisconnected(name);
                logger.info("disconnected: " + name);
            }
            super.channelInactive(ctx);
        }

        private void authorize(ChannelHandlerContext ctx, JSONObject msg) {
            if (channels.size() >= 2) {
                shutdown(ctx);
                return;
            }

            MessageType type = MessageType.get(msg.getInt("type"));
            if (type == MessageType.AUTH) {
                String name = msg.getString("name");
                if (!name.isEmpty() && !channels.keySet().contains(name)) {
                    isAuthorized = true;
                    channels.put(name, ctx.channel());
                    this.name = name;
                    ctx.writeAndFlush(
                        new JSONObject()
                            .put("type", MessageType.AUTH.code())
                            .put("name", name)
                    );
                    firePlayerConnected(name);
                    logger.info("connected: " + name);
                } else {
                    sendError(ctx, ErrorType.NAME_ERROR.code);
                }
            }
        }

        private void handleRequest(ChannelHandlerContext ctx, JSONObject msg) {
            MessageType type = MessageType.get(msg.getInt("type"));
            if (type == MessageType.MESSAGE) {
                channels.values().forEach(c -> c.writeAndFlush(msg));
            } else if (type == MessageType.UPDATE && msg.getString("name").equals(name)) {
                PlayerUpdate update = PlayerUpdate.fromJson(msg);
                firePlayerUpdated(update);
            }
        }

        private void sendError(ChannelHandlerContext ctx, Object error) {
            JSONObject msg = new JSONObject();
            msg.put("type", MessageType.ERROR.code());
            msg.put("error", error);
            ctx.writeAndFlush(msg);
        }

        private void shutdown(ChannelHandlerContext ctx) {
            sendError(ctx, ErrorType.SERVER_FULL.code);
            ctx.channel()
                .write(new CloseWebSocketFrame())
                .addListener(ChannelFutureListener.CLOSE);
        }
    }

    private static enum MessageType {
        MESSAGE(0), UPDATE(1), AUTH(2), ERROR(3);

        private int code;

        MessageType(int code) {
            this.code = code;
        }

        public int code() {
            return code;
        }

        public static MessageType get(int code) {
            switch (code) {
                case 0: return MESSAGE;
                case 1: return UPDATE;
                case 2: return AUTH;
                case 3: return ERROR;
                default: throw new IllegalArgumentException();
            }
        }
    }

    private static enum ErrorType {
        NAME_ERROR(0), SERVER_FULL(1);

        public final int code;

        ErrorType(int code) {
            this.code = code;
        }

        public static ErrorType get(int code) {
            switch (code) {
                case 0: return NAME_ERROR;
                case 1: return SERVER_FULL;
                default: throw new IllegalArgumentException();
            }
        }
    }
}
