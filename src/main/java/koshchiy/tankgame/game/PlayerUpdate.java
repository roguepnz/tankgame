package koshchiy.tankgame.game;

import org.json.JSONObject;


public class PlayerUpdate {
    private String name;
    private Direction direction;
    private boolean isFiring;
    private boolean isMoving;

    public PlayerUpdate(String name, Direction direction, boolean isFiring, boolean isMoving) {
        this.name = name;
        this.direction = direction;
        this.isFiring = isFiring;
        this.isMoving = isMoving;
    }

    public String name() {
        return name;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public boolean isFiring() {
        return isFiring;
    }

    public Direction direction() {
        return direction;
    }

    public static PlayerUpdate fromJson(JSONObject json) {
        return new PlayerUpdate(
            json.getString("name"),
            Direction.get(json.getInt("direction")),
            json.getBoolean("isFiring"),
            json.getBoolean("isMoving")
        );
    }

    @Override
    public String toString() {
        return "PlayerUpdate{" +
            "name='" + name + '\'' +
            ", direction=" + direction +
            ", isFiring=" + isFiring +
            ", isMoving=" + isMoving +
            '}';
    }
}
