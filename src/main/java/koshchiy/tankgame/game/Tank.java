package koshchiy.tankgame.game;

public class Tank {
    private String name;
    private int health;
    private int x;
    private int y;
    private Direction direction;
    private boolean isFiring;
    private boolean canFire;

    public Tank(String name, int x, int y, Direction direction) {
        this.name = name;
        this.health = 100;
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.canFire = true;
    }

    public String name() {
        return name;
    }

    public boolean canFire() {
        return canFire;
    }

    public void setCanFire(boolean canFire) {
        this.canFire = canFire;
    }

    public boolean isFiring() {
        return isFiring;
    }

    public void setFiring(boolean isFiring) {
        this.isFiring = isFiring;
    }

    public int health() {
        return health;
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean isDead() {
        return health <= 0;
    }

    public boolean tryHit(Bullet bullet) {
        boolean hit = false;
        if (!bullet.shooter().equals(name)) {
            int diffx = Math.abs(bullet.x() - this.x);
            int diffy = Math.abs(bullet.y() - this.y);
            hit = diffx < 2 && diffy < 2;

            if (hit && this.health > 0) {
                this.health -= 10;
            }
        }
        return hit;
    }

    public Bullet makeBullet() {
        this.canFire = false;
        this.setFiring(true);
        return new Bullet(this.x, this.y, this.direction, this.name);
    }

    public void move(int value, TankSnapshot s) {
        switch (direction) {
            case RIGHT: x += value; s.setX(x); break;
            case LEFT: x -= value; s.setX(x); break;
            case DOWN: y += value; s.setY(y); break;
            case UP: y -= value; s.setY(y); break;
        }
    }

    @Override
    public String toString() {
        return "Tank{" +
            "name='" + name + '\'' +
            ", health=" + health +
            ", x=" + x +
            ", y=" + y +
            ", direction=" + direction +
            '}';
    }
}
