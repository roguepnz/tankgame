package koshchiy.tankgame.game;


public enum Direction {
    UP(0), DOWN(1), LEFT(2), RIGHT(3);

    private int code;

    Direction(int code) {
        this.code = code;
    }

    public int code() {
        return code;
    }

    public static Direction get(int code) {
        switch (code) {
            case 0: return UP;
            case 1: return DOWN;
            case 2: return LEFT;
            case 3: return RIGHT;
            default: throw new IllegalArgumentException();
        }
    }
}
