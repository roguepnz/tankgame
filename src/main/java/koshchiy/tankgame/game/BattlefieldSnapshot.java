package koshchiy.tankgame.game;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class BattlefieldSnapshot extends JSONObject {
    private Map<String, TankSnapshot> connected;
    private Set<String> disconnected;
    private Map<String, TankSnapshot> updated;

    public BattlefieldSnapshot() { }

    public BattlefieldSnapshot(Battlefield b) {
        b.tanks().values().forEach(t -> addConnected(new TankSnapshot(t)));

        JSONObject frags = new JSONObject();
        b.frags().entrySet().forEach(e -> frags.put(e.getKey(), e.getValue()));
        this.put("fragmap", frags);
    }

    public Set<String> connectedNames() {
        return connected().keySet();
    }

    public boolean isEmpty() {
        return this.length() == 0;
    }

    public TankSnapshot getOrCreateUpdated(String name) {
        TankSnapshot snapshot = updated().get(name);
        if (snapshot == null) {
            snapshot = new TankSnapshot(name);
            updated().put(name, snapshot);
            JSONArray("updated").put(snapshot);
        }
        return snapshot;
    }

    public void addDisconnected(String name) {
        if (!disconnected().contains(name)) {
            disconnected().add(name);
            JSONArray("disconnected").put(name);
        }
    }

    public void addConnected(TankSnapshot snapshot) {
        if (!connected().containsKey(snapshot.name())) {
            connected().put(snapshot.name(), snapshot);
            JSONArray("connected").put(snapshot);
        }
    }

    public void addFrag(String name) {
        JSONArray("frags").put(name);
    }


//    public void clear() {
//        Iterator it = this.keys();
//        while (it.hasNext()) {
//            it.next();
//            it.remove();
//        }
//
//        connected().clear();
//        disconnected().clear();
//        updated().clear();
//    }

    private JSONArray JSONArray(String key) {
        JSONArray array = this.optJSONArray(key);
        if (array == null) {
            array = new JSONArray();
            this.put(key, array);
        }
        return array;
    }

    private Map<String, TankSnapshot> connected() {
        if (connected == null) {
            connected = new HashMap<>();
        }
        return connected;
    }

    private Set<String> disconnected() {
        if (disconnected == null) {
            disconnected = new HashSet<>();
        }
        return disconnected;
    }

    private Map<String, TankSnapshot> updated() {
        if (updated == null) {
            updated = new HashMap<>();
        }
        return updated;
    }
}