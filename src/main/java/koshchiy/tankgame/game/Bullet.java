package koshchiy.tankgame.game;

public class Bullet {
    private int x;
    private int y;
    private Direction direction;
    private String shooter;

    public Bullet(int x, int y, Direction direction, String shooter) {
        this.x = x;
        this.y = y;
        this.direction = direction;
        this.shooter = shooter;
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    public String shooter() {
        return shooter;
    }

    public Direction direction() {
        return direction;
    }

    public void move(int value) {
        switch (this.direction()) {
            case UP: y -= value; break;
            case DOWN: y += value; break;
            case LEFT: x -= value; break;
            case RIGHT: x += value; break;
        }
    }
}
