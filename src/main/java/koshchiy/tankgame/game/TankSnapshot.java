package koshchiy.tankgame.game;

import org.json.JSONObject;


public class TankSnapshot extends JSONObject {

    public TankSnapshot(String name) {
        this.put("name", name);
    }

    public TankSnapshot(Tank tank) {
        this(tank.name());

        this.setHealth(tank.health())
            .setX(tank.x())
            .setY(tank.y())
            .setFiring(tank.isFiring())
            .setDirection(tank.getDirection());
    }

    public String name() {
        return this.getString("name");
    }

    public TankSnapshot setHealth(int health) {
        this.put("health", health);
        return this;
    }

    public TankSnapshot setX(int x) {
        this.put("x", x);
        return this;
    }

    public TankSnapshot setY(int y) {
        this.put("y", y);
        return this;
    }

    public TankSnapshot setFiring(boolean isFiring) {
        this.put("isFiring", isFiring);
        return this;
    }

    public TankSnapshot setDirection(Direction direction) {
        this.put("direction", direction.code());
        return this;
    }

    public  TankSnapshot setFrags(int value) {
        this.put("frags", value);
        return this;
    }
}
