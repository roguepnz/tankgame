package koshchiy.tankgame.game;

import koshchiy.tankgame.Constants;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

/**
 * Main game class. Performs updates of all game objects.
 */
public class Battlefield {
    private final int width;
    private final int height;

    private final Map<String, Tank> tanks;
    private final Map<String, Integer> deadCounts;
    private final Map<String, Integer> reloadCounts;
    private final Map<String, Integer> frags;
    private final LinkedList<Bullet> bullets;

    private final Queue<String> connected;
    private final Queue<String> disconnected;
    private final Queue<PlayerUpdate> updated;

    /**
     * Creates the new instance of battlefield.
     *
     * @param width width of the battlefield
     * @param height height of the battlefield
     */
    public Battlefield(int width, int height) {
        this.width = width;
        this.height = height;

        this.tanks = new HashMap<>();
        this.bullets = new LinkedList<>();
        this.deadCounts = new HashMap<>();
        this.reloadCounts = new HashMap<>();
        this.frags = new HashMap<>();

        this.connected = new ConcurrentLinkedQueue<>();
        this.disconnected = new ConcurrentLinkedQueue<>();
        this.updated = new ConcurrentLinkedQueue<>();
    }

    /**
     * @return names of the connected players
     */
    public Set<String> names() {
        return tanks.keySet();
    }

    /**
     * @return
     */
    public Map<String, Tank> tanks() {
        return tanks;
    }

    public Map<String, Integer> frags() {
        return frags;
    }

    /**
     * Adds the {@link koshchiy.tankgame.game.PlayerUpdate update} to the processing queue.
     * Invocation of the method is thread-safe.
     *
     * @param update player update
     */
    public void addUpdate(PlayerUpdate update) {
        this.updated.add(update);
    }

    /**
     * Adds the new player name to the processing queue.
     * Invocation of the method is thread-safe.
     *
     * @param name name of the player
     */
    public void addConnected(String name) {
        this.connected.add(name);
    }

    /**
     * Adds the disconnected player name to the processing queue.
     * Invocation of the method is thread-safe.
     *
     * @param name name of the disconnected player
     */
    public void addDisconnected(String name) {
        this.disconnected.add(name);
    }

    /**
     * Performs update of the game world per tick.
     *
     * @return {@link koshchiy.tankgame.game.BattlefieldSnapshot snapshot}
     */
    public BattlefieldSnapshot tick() {
        BattlefieldSnapshot snapshot = new BattlefieldSnapshot();

        processDead(snapshot);
        processUpdated(snapshot);
        processBullets(snapshot);
        processReloads();
        processDisconnected(snapshot);
        processConnected(snapshot);

        return snapshot;
    }

    private void processDisconnected(BattlefieldSnapshot s) {
        iterateQueue(disconnected, name -> {
            tanks.remove(name);
            deadCounts.remove(name);
            reloadCounts.remove(name);
            frags.remove(name);

            s.addDisconnected(name);
        });
    }

    private void processConnected(BattlefieldSnapshot s) {
        iterateQueue(connected, name -> {
            frags.put(name, 0);
            s.addConnected(new TankSnapshot(respawnTank(name)));
        });
    }

    private void processUpdated(BattlefieldSnapshot s) {
        Iterator<PlayerUpdate> it = updated.iterator();
        Set<String> viewed = new HashSet<>();
        while (it.hasNext()) {
            PlayerUpdate update = it.next();
            String name = update.name();
            Tank tank = tanks.get(name);
            if (!viewed.contains(name)) {
                if (!tank.isDead()) {
                    if (update.isFiring() && tank.canFire()) {
                        bullets.addFirst(tank.makeBullet());
                        reloadCounts.put(name, Constants.RELOAD_SPEED);
                        s.getOrCreateUpdated(name).setFiring(true);
                    }
                    if (update.isMoving()) {
                        Direction dir = update.direction();
                        Direction tankDir = tank.getDirection();
                        if (dir == tankDir && canMove(tank) ) {
                            tank.move(Constants.TANK_SPEED, s.getOrCreateUpdated(name));
                        } else {
                            tank.setDirection(update.direction());
                            s.getOrCreateUpdated(name)
                                .setDirection(update.direction());
                        }
                    }
                }
                it.remove();
                viewed.add(name);
            }
        }
    }

    private void processReloads() {
        Iterator<String> it = reloadCounts.keySet().iterator();
        while (it.hasNext()) {
            String current = it.next();
            int count = reloadCounts.get(current);
            if (count == 0) {
                it.remove();
                tanks.get(current).setCanFire(true);
            } else {
                reloadCounts.replace(current, count - 1);
            }
        }
    }

    /**
     * Performs update of each dead {@link koshchiy.tankgame.game.Tank tank} on the battlefield.
     */
    private void processDead(BattlefieldSnapshot s) {
        Iterator<String> it = deadCounts.keySet().iterator();
        while (it.hasNext()) {
            String current = it.next();
            int count = deadCounts.get(current);
            if (count == 0) {
                it.remove();
                Tank tank = respawnTank(current);
                s.getOrCreateUpdated(current)
                    .setHealth(tank.health())
                    .setX(tank.x())
                    .setY(tank.y())
                    .setDirection(tank.getDirection());
            } else {
                deadCounts.replace(current, count - 1);
            }
        }
    }

    /**
     * Performs update of each {@link koshchiy.tankgame.game.Bullet bullet} on the battlefield.
     */
    private void processBullets(BattlefieldSnapshot s) {
        Iterator<Bullet> iterator = bullets.iterator();
        while (iterator.hasNext()) {
            Bullet current = iterator.next();

            current.move(Constants.BULLET_SPEED);

            Optional<Tank> victim = tanks.values()
                .stream()
                .filter(t -> !t.isDead() && t.tryHit(current))
                .findFirst();

            if (canBulletFly(current) || victim.isPresent()) {
                iterator.remove();
            }
            victim.ifPresent(v -> {
                if (v.isDead()) {
                    deadCounts.put(v.name(), Constants.DEAD_TIME);
                    addFrag(s, current.shooter());
                }
                s.getOrCreateUpdated(v.name()).setHealth(v.health());
            });
        }
    }

    /**
     * Adds frag to the player.
     *
     * @param s
     * @param name name of the player
     */
    private void addFrag(BattlefieldSnapshot s, String name) {
        int count = frags.get(name);
        frags.put(name, count + 1);
        s.addFrag(name);
    }

    /**
     * Indicates whether the specified {@link koshchiy.tankgame.game.Bullet bullet}
     * can fly on the battlefield.
     */
    private boolean canBulletFly(Bullet bullet) {
        int x = bullet.x();
        int y = bullet.y();

        return x > this.width || x < 0 || y > this.height || y < 0;
    }

    /**
     * Creates new instance of the player {@link koshchiy.tankgame.game.Tank tank} and places it
     * on a random position in the battlefield.
     *
     * @param name name of the player
     */
    private Tank respawnTank(String name) {
        int minY = 1;
        int maxY = this.width - 2;
        int minX = 1;
        int maxX = this.height - 2;

        int x = minX + (int)(Math.random() * ((maxX - minX) + 1));
        int y = minY + (int)(Math.random() * ((maxY - minY) + 1));
        Direction direction = Direction.get((int)(Math.random() * 4));

        Tank tank = new Tank(name, x, y, direction);
        this.tanks.put(name, tank);

        return tank;
    }

    /**
     * Indicates whether the specified {@link koshchiy.tankgame.game.Tank tank}
     * can move on the battlefield.
     */
    private boolean canMove(Tank tank) {
        Direction d = tank.getDirection();
        return !((d == Direction.DOWN && tank.y() >= this.height - 2) ||
            (d == Direction.UP && tank.y() <= 1) ||
            (d == Direction.RIGHT && tank.x() >= this.width - 2) ||
            (d == Direction.LEFT && tank.x() <= 1));
    }

    /**
     * Iterates on the queue with removing each item from the collection
     * and performing specified action on it.
     *
     * @param queue queue to iterate
     * @param action action to perform
     * @param <T> type of the item
     */
    private <T> void iterateQueue(Queue<T> queue, Consumer<T> action) {
        Iterator<T> iterator = queue.iterator();
        while (iterator.hasNext()) {
            T current = iterator.next();
            iterator.remove();
            action.accept(current);
        }
    }
}