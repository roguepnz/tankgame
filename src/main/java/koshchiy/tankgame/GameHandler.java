package koshchiy.tankgame;

import koshchiy.tankgame.game.Battlefield;
import koshchiy.tankgame.game.BattlefieldSnapshot;
import koshchiy.tankgame.game.PlayerUpdate;
import koshchiy.tankgame.server.Server;
import koshchiy.tankgame.server.ServerListener;

import java.util.Set;

/**
 * A main game handler. Listens for server events.
 */
public class GameHandler implements ServerListener {
    private final Server server;
    private final Battlefield battlefield;

    public GameHandler(Server server, Battlefield battlefield) {
        this.server = server;
        this.battlefield = battlefield;
    }

    /**
     * Starts {@link koshchiy.tankgame.server.Server server} listening loop and
     *
     * @throws InterruptedException
     */
    public void run() throws InterruptedException {
        server.run();

        while(true) {
            tick();
            Thread.sleep(Constants.TICK_RATE);
        }
    }

    private void tick() {
        BattlefieldSnapshot snapshot = battlefield.tick();
        if (!snapshot.isEmpty()) {
            BattlefieldSnapshot forConnected = null;
            Set<String> connectedNames = snapshot.connectedNames();

            if (!connectedNames.isEmpty()) {
                forConnected = new BattlefieldSnapshot(battlefield);
            }

            for (String name : battlefield.names()) {
                if (connectedNames.contains(name)) {
                    server.sendSnapshot(name, forConnected);
                } else {
                    server.sendSnapshot(name, snapshot);
                }
            }
        }
    }

    @Override
    public void playerUpdated(PlayerUpdate update) {
        this.battlefield.addUpdate(update);
    }

    @Override
    public void playerConnected(String name) {
        this.battlefield.addConnected(name);
    }

    @Override
    public void playerDisconnected(String name) {
        this.battlefield.addDisconnected(name);
    }
}
