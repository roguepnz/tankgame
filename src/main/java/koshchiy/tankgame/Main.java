package koshchiy.tankgame;

import koshchiy.tankgame.game.Battlefield;
import koshchiy.tankgame.server.NettyServer;
import koshchiy.tankgame.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {

    private final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException, InterruptedException {
        Properties properties = new Properties();
        properties.load(new FileInputStream("src/nonpackaged/config.properties"));
//        properties.load(new FileInputStream("config.properties"));

        int port = Integer.parseInt(properties.getProperty("port"));
        String path = properties.getProperty("web");

        Server server = new NettyServer(port, path);
        Battlefield battlefield = new Battlefield(50, 50);
        GameHandler gameHandler = new GameHandler(server, battlefield);
        server.addListener(gameHandler);

        logger.info("started at " + port);
        gameHandler.run();
    }
}