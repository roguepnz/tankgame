package koshchiy.tankgame;


public class Constants {

    public final static int TICK_RATE = 1000/30;

    public final static int BULLET_SPEED = 3;

    public final static int TANK_SPEED = 1;

    public final static int WIDTH = 50;

    public final static int HEIGHT = 50;

    public final static int RELOAD_SPEED = 4;

    public final static int DEAD_TIME = 15;
}
