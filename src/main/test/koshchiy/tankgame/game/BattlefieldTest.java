package koshchiy.tankgame.game;

import koshchiy.tankgame.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class BattlefieldTest {

    @Test
    public void testTankConnection() {
        Battlefield battlefield = new Battlefield(Constants.WIDTH, Constants.HEIGHT);
        battlefield.addConnected("tank#1");
        BattlefieldSnapshot snapshot = battlefield.tick();

        TankSnapshot s = (TankSnapshot)snapshot.getJSONArray("connected").get(0);
        int x = s.getInt("x");
        int y = s.getInt("y");

        assertEquals(1, battlefield.tanks().size());
        assertTrue(!snapshot.isEmpty());
        assertEquals("tank#1", s.name());
        assertTrue( x > 0 && x < Constants.WIDTH);
        assertTrue( y > 0 && y < Constants.HEIGHT);
    }

    @Test
    public void testTankDisconnection() {
        Battlefield battlefield = new Battlefield(Constants.WIDTH, Constants.HEIGHT);
        battlefield.addConnected("tank#1");
        battlefield.tick();

        battlefield.addDisconnected("tank#1");
        BattlefieldSnapshot s = battlefield.tick();

        String name = s.getJSONArray("disconnected").getString(0);

        assertTrue(battlefield.tanks().isEmpty());
        assertEquals("tank#1", name);
    }
}