var LinkedList = function() {
  this.head = null;
  this.length = 0;
};

LinkedList.prototype = {
  push: function(item) {
    var node = {
      item: item,
      next: null,
      prev: null
    };

    if (this.head == null) {
      this.head = node;
    } else {
      var current = this.head;
      this.head = node;
      this.head.next = current;
      current.prev = this.head;

      this.length++;
    }
  },

  forEach: function(action) {
    if (this.head) {
      var current = this.head;
      action(current);
      while(current.next) {
        current = current.next;
        action(current);
      }
    }
  },

  remove: function(node) {
    if (this.head) {
      var prev = node.prev;
      var next = node.next;

      if (prev) {
        prev.next = next;
      } else {
        this.head = next;
      }

      if (next) {
        next.prev = prev;
      }

      this.length--;
    }
  }
};

//var list = new LinkedList();
//
//list.push(1);
//list.push(2);
//list.push(3);
//list.push(4);
//list.push(5);
//list.push(6);
//
//list.forEach(function(node) {
//  var num = node.item;
//
//  console.log(num);
//  list.remove(node);
//  printList(list);
//});
//
//
//function printList(list) {
//  var arr = [];
//  list.forEach(function(node) {
//    arr.push('->' + node.item);
//  });
//
//  console.log(arr.join(''));
//}
//
