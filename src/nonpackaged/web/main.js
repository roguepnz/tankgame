var MessageType = {
  '0': 'MESSAGE',
  '1': 'UPDATE',
  '2': 'AUTH',
  '3': 'ERROR'
};

var ErrorType = {
  '0': 'NAME_ERROR',
  '1': 'SERVER_FULL'
};

var Direction = {
  '0': [0, 1, 0,
        1, 1, 1,
        1, 0, 1], // up

  '1': [1, 0, 1,
        1, 1, 1,
        0, 1, 0], // down

  '2': [0, 1, 1,
        1, 1, 0,
        0, 1, 1], // left

  '3': [1, 1, 0,
        0, 1, 1,
        1, 1, 0] // right
};

var Tank = function(name, x, y, direction, health, isFiring) {
  this.name = name;
  this.x = x;
  this.y = y;
  this.direction = direction;
  this.health = health;
  this.isFiring = isFiring;
};

Tank.prototype = {
  tryHit: function(bullet) {
    var xdiff = Math.abs(bullet.x - this.x);
    var ydiff = Math.abs(bullet.y - this.y);

    return xdiff < 2 && ydiff < 2;
  },

  isDead: function() {
    return this.health <= 0;
  }
};

var Bullet = function(x, y, direction) {
  this.x = x;
  this.y = y;
  this.direction = direction;
};

var Battlefield = function(width, height, cellsize) {
  this.width = width;
  this.height = height;
  this.cellsize = cellsize;

  this.canvas = document.getElementById('field');
  this.canvas.width  = width * cellsize;
  this.canvas.height = height * cellsize;
  this.ctx = this.canvas.getContext('2d');
  this.ctx.font = "12px Arial";
  this.bullets = new LinkedList();
};

Battlefield.prototype = {
  playerName: '',
  tankSize: 3,
  tanks:{},

  playerTank: function() {
    return this.tanks[this.playerName];
  },

  updateTank: function(data) {
    var tank = this.tanks[data.name];
    if (tank.name != this.playerName || tank.isDead()) {
      if (data.x != undefined) tank.x = data.x;
      if (data.y != undefined) tank.y = data.y;
      if (data.direction != undefined) tank.direction = data.direction;
    }
    if (data.health != undefined) tank.health = data.health;

    if (data.isFiring != undefined) tank.isFiring = data.isFiring;
    else tank.isFiring = false;

    if (tank.isFiring) {
      this.bullets.push(new Bullet(tank.x, tank.y, tank.direction));
    }
  },

  addTank: function(data) {
    this.tanks[data.name] = new Tank(
      data.name,
      data.x,
      data.y,
      data.direction,
      data.health,
      data.isFiring
    );
  },

  deleteTank: function(name) {
    delete this.tanks[name];
  },

  redraw: function() {
    var that = this;
    this._clear();
    for (name in this.tanks) {
      this._drawTank(this.tanks[name]);
      this._drawHealthAndName(this.tanks[name]);
    }
    this.bullets.forEach(function(node) {
      var bullet = node.item;
      that._drawBullet(bullet);
    });
  },

  _clear: function() {
    this.ctx.fillStyle = 'rgb(255,255,255)';
    var w = this.cellsize * this.width;
    var h = this.cellsize * this.height;
    this.ctx.fillRect(0, 0, w, h);
  },

  _drawHealthAndName: function(tank) {
    var ctx  = this.ctx;
    var x = tank.x * this.cellsize - 10;
    var y = tank.y * this.cellsize + 25;
    var health = tank.health;

//    ctx.fillStyle="#a5f5a5";
    ctx.fillStyle="#e00000";

    ctx.fillRect( x, y, health/3, 5 );

    ctx.fillStyle="#000";
    ctx.font = "8px minecraftiaregular";
    ctx.textBaseline = "bottom";
    ctx.fillText(tank.name, x, y + 20);
  },

  _drawTank: function(tank) {
    var startx = (tank.x - 1) * this.cellsize;
    var starty = (tank.y - 1) * this.cellsize;

    if (tank.isDead()) {
      this.ctx.fillStyle = '#de0000';
    } else {
      if (tank.name == this.playerName) {
        this.ctx.fillStyle = '#3b5998';
      } else {
        this.ctx.fillStyle = '#95b070';
      }
    }

    for (i = 0; i < this.tankSize; i++) {
      for (j = 0; j < this.tankSize; j++) {
        var cell = Direction[tank.direction][j * this.tankSize + i];
        if (cell) {
          var x = startx + i * this.cellsize + 1;
          var y = starty + j * this.cellsize + 1;

          this.ctx.fillRect(x, y, this.cellsize - 1, this.cellsize - 1);
        }
      }
    }
  },

  _drawBullet: function(bullet) {
    var startx = (bullet.x) * this.cellsize + 1;
    var starty = (bullet.y) * this.cellsize + 1;
    this.ctx.fillStyle = 'rgb(110, 110, 110)';
    this.ctx.fillRect(startx, starty, this.cellsize - 1, this.cellsize - 1);
  },

  updateBullets: function() {
    var that = this;
    this.bullets.forEach(function(node) {
      var b = node.item;
      var d = b.direction;
      switch (d) {
        case 0: b.y -= 3; break;
        case 1: b.y += 3; break;
        case 2: b.x -= 3; break;
        case 3: b.x += 3; break;
      }

      for (i in that.tanks) {
        var t = that.tanks[i];
        if (b.x > that.width || b.y > that.height || b.x < 0 || b.y < 0 || t.tryHit(b) ) {
          that.bullets.remove(node);
        }
      }
    });
  },

  updatePlayerTank: function(update) {
    var tank = this.playerTank();

    if (!tank.isDead()) {
      tank.isMoving = update.isMoving;
      if (tank.isMoving) {

        if (tank.direction == update.direction && this._canMove(tank)) {
          switch (update.direction) {
            case 0: tank.y -= 1; break;
            case 1: tank.y += 1; break;
            case 2: tank.x -= 1; break;
            case 3: tank.x += 1; break;
          }
        } else {
          tank.direction = update.direction;
        }
      }
    }
  },

  _canMove: function (tank) {
    var d = tank.direction;
    return !((d == '1' && tank.y + 2 >= this.height) ||
      (d == '0' && tank.y - 1 <= 0) ||
      (d == '3' && tank.x + 2 >= this.width) ||
      (d == '2' && tank.x - 1 <= 0));
  }
};

var Input = function() {
  var that = this;
  window.addEventListener('keydown', function(e) {
    that._onKeyEvent.call(that, e, true);
  }, false);

  window.addEventListener('keyup', function(e) {
    that._onKeyEvent.call(that, e, false);
  }, false);
};

Input.prototype = {
  keys: {
    '38': { name: 'UP', pressed: false, direction: 0 },
    '40': { name: 'DOWN', pressed: false, direction: 1 },
    '37': { name: 'LEFT', pressed: false, direction: 2 },
    '39': { name: 'RIGHT', pressed: false, direction: 3 },
    '32': { name: 'FIRE', pressed: false }
  },

  _onKeyEvent: function (e, pressed) {
    var code = e.keyCode;
    if (this.keys[code]) {
      this.keys[code].pressed = pressed;
    }
  },

  isFiring: function() {
    for (i in this.keys) {
      var key = this.keys[i];
      if (key.pressed && key.name == 'FIRE') {
        return true;
      }
    }
    return false;
  },

  isMoving: function() {
    for (i in this.keys) {
      var key = this.keys[i];
      if (key.pressed && key.name != 'FIRE') {
        return true;
      }
    }
    return false;
  },

  direction: function() {
    for (i in this.keys) {
      var key = this.keys[i];
      if (key.pressed && key.name != 'FIRE') {
        return key.direction;
      }
    }
    return undefined;
  },

  isPressed: function() {
    for (i in this.keys) {
      var key = this.keys[i];
      if (key.pressed) {
        return true;
      }
    }
    return false;
  }
};

var Client = function(host, listener) {
  this.listener = listener;
  this.socket = new WebSocket(host);

  var that = this;
  this.socket.onmessage = function(msg) {
    that._onmessage.call(that, msg);
  };

  this.socket.onopen = function() {
    that.listener.onConnect();
  }
};

Client.prototype = {
  _onmessage: function(msg) {
    console.log("received: " + msg.data);
    var data = JSON.parse(msg.data);
    var type = data.type;
    switch (MessageType[type]) {
      case 'ERROR': this.listener.onError(data); break;
      case 'AUTH': this.listener.onAuth(data.name); break;
      case 'UPDATE' : this.listener.onUpdate(data); break;
    }
  },

  send: function(data) {
    this.socket.send(JSON.stringify(data));
  }
};

var Update = function(name, isMoving, isFiring, direction) {
  this.type = 1;
  this.name = name;
  this.isFiring = isFiring;
  this.isMoving = isMoving;
  this.direction = direction;
};

var Scoreboard = function() {
  this.frags = [];
  this.needUpdate = false;
  this.view = document.getElementById('scoreboard');
};

Scoreboard.prototype = {
  addOrUpdate: function(name)  {
    if (this.frags[name] != undefined) {
      this.frags[name] += 1;
    } else {
      this.frags[name] = 0;
    }
    this.needUpdate = true;
  },

  remove: function(name) {
    delete this.frags[name];
    this.needUpdate = true;
  },

  updateView: function() {
    if (this.needUpdate) {
      this.view.innerHTML = this._tpl(this._sorted(this.frags));
    }
    this.needUpdate = false;
  },

  _tpl: function(frags) {
    var arr = [];
    frags.forEach(function(item) {
      arr.push("<tr>");
      arr.push("<td>" + item.key + "</td>");
      arr.push("<td>" + item.value + "</td>");
      arr.push("</tr>");
    });
    return arr.join("");
  },

  _sorted: function(frags) {
    var arr = [];
    for (i in frags) {
      arr.push({ key: i, value: frags[i] });
    }
    return arr.sort(function(a, b) { return b.value - a.value; });
  }
};

function main() {
  var battlefield = new Battlefield(50, 50, 10);
  var scoreBoard = new Scoreboard();
  var input = new Input;

  var client = new Client('ws://' + window.location.host + '/websocket', {
    onUpdate: function(data) {
      if (data.updated) {
        data.updated.forEach(function(d) { battlefield.updateTank(d); });
      }
      if (data.disconnected) {
        data.disconnected.forEach(function(t) {
          battlefield.deleteTank(t);
          scoreBoard.remove(t);
        });
      }
      if (data.connected) {
        data.connected.forEach(function(t) {
          scoreBoard.addOrUpdate(t.name);
          battlefield.addTank(t);
        });
      }
      if (data.frags) {
        data.frags.forEach(function(n) { scoreBoard.addOrUpdate(n); });
      }

      if (data.fragmap) {
        scoreBoard.frags = data.fragmap;
      }
    },

    onError: function(data) {
      var msg = JSON.stringify(data);
      console.log("error:" + msg);
      // TODO shitcode
      if (data.error == 0) {
        var name = auth(true);
        client.send({ type: 2, name: name });
      } else if (data.error == 1) {
        alert('sorry, server is full, try later.');
      }
    },

    onAuth: function(name) {
      battlefield.playerName = name;
      loop(input, client, battlefield, scoreBoard);
    },

    onConnect: function() {
      var name = auth(false);
      client.send({ type: 2, name: name });
    }
  });
}

function loop(input, client, battlefield, scoreBoard) {
  if (input.isPressed()) {
    var name = battlefield.playerName;
    var isMoving = input.isMoving();
    var isFiring = input.isFiring();
    var direction = isMoving ?
      input.direction() : battlefield.playerTank().direction;

    var update = new Update(name, isMoving, isFiring, direction);

    client.send(update);
    battlefield.updatePlayerTank(update);
  }
  battlefield.updateBullets();
  battlefield.redraw();
  scoreBoard.updateView();
  setTimeout(function() { loop(input, client, battlefield, scoreBoard); }, 1000/30);
}

function auth(isPrevFailed) {
  var message = isPrevFailed ? 'name is busy or incorrect, try again:' : 'enter your name:';
  return prompt(message);
}

window.onload = main;